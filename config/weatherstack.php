
<?php

return [
    'weatherstack' => [
        'base_uri' => env('WEATHERSTACK_BASE_URI'),
        'access_key' => env('WEATHERSTACK_ACCESS_KEY'),
    ],
];
