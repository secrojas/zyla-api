<?php

use App\Http\Controllers\Api\WeatherCityController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {
    Route::get('current', [WeatherCityController::class, 'showWeather']);
});
