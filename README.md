<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>



# Set up the  Zyla API Challenge

For this project was used Laravel 9 with PHP version 8.1.

### Instructions for setting up the project:

- clone the repo.
- create local database, use the name <b>zyla-api</b> just like in the .env.example file.
- go inside de repo path and run: <b>composer install</b>
- inside the project <b>copy the .env.example</b> file and <b>rename</b> as <b>.env</b>.
- in the .env file <b>set-up your DB Credentials</b> (database name, user and password).
- in the .env file <b>set-up the Weatherslack API Access Key</b>, the name of the variable is <b>WEATHERSTACK_ACCESS_KEY</b>. You can use the same as the example of the .env.example file.
- in the .env file <b>set-up the Weatherslack BASE URI</b>, the name of the variable is <b>WEATHERSTACK_BASE_URI</b>. You must use the same as the .env.example file: <b>http://api.weatherstack.com/current</b>
- run <b>php artisan key:generate</b>
- run the migrations: <b>php artisan migrate</b>
- run <b>php artisan serve</b>

_Once the project is running, you can access by Postman or similar, the endpoint to get the weather of the wanted city._

### Example of the endpoint:

- http://127.0.0.1:8000/api/v1/current?query=New%20York

- [_Here you can see the description of the challenge._](docs/challenge.md)


