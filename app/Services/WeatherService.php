<?php

namespace App\Services;

use App\Models\WeatherCity;
use Illuminate\Support\Facades\Http;

class WeatherService
{
    public function getExternalWeather(string $query)
    {
        $data = Http::get(config('weatherstack.weatherstack.base_uri'), [
            'access_key' => config('weatherstack.weatherstack.access_key'),
            'query' => $query
        ]);

        $response = json_decode($data->body());
        
        return $response;
    }

    public function insertWeatherData(object $obj)
    {
        $country = validateCountry($obj->location->country);
        $region = validateRegion($obj->location->region);
        ;

        $weatherCity = WeatherCity::updateOrCreate(
            [
                'name' => $obj->location->name,
            ],
            [
            'type' => $obj->request->type,
            'query' =>  $region->name .', '.$country->name,
            'language' =>  $obj->request->language,
            'unit' =>  $obj->request->unit,
            'name' => $obj->location->name,
            'country_id' => $country->id,
            'region_id' =>$region->id,
            'lat' => $obj->location->lat,
            'lon' => $obj->location->lon,
            'timezone_id' => $obj->location->timezone_id,
            'localtime' => $obj->location->localtime,
            'localtime_epoch' => $obj->location->localtime_epoch,
            'utc_offset' => $obj->location->utc_offset,
            'observation_time' => $obj->current->observation_time,
            'temperature' =>$obj->current->temperature,
            'weather_code' => $obj->current->weather_code,
            'weather_icons' =>
                $obj->current->weather_icons[0]
            ,
            'weather_descriptions' =>
                $obj->current->weather_descriptions[0]
            ,
            'wind_speed' => $obj->current->wind_speed,
            'wind_degree' => $obj->current->wind_degree,
            'wind_dir' => $obj->current->wind_dir,
            'pressure' => $obj->current->pressure,
            'precip' => $obj->current->precip,
            'humidity' => $obj->current->humidity,
            'cloudcover' => $obj->current->cloudcover,
            'feelslike' => $obj->current->feelslike,
            'uv_index' => $obj->current->uv_index,
            'visibility' => $obj->current->visibility,
            'is_day' => $obj->current->is_day
        ]
        );
        
        return $weatherCity;
    }
}
