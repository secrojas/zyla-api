<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WeatherCity;

use App\Services\WeatherService;

class WeatherCityController extends Controller
{
    private WeatherService $weatherService;

    public function __construct(WeatherService $weatherService)
    {
        $this->weatherService = $weatherService;
    }

    public function showWeather(Request $request)
    {
        $query = $request['query'];
        
        $response = null;

        if (!$query) {
            return missingQueryResponse();
        } else {
            $dataCity = WeatherCity::where('name', $query)->first();
            
            if ($dataCity &&  validateHours($dataCity)) {
                $response = formatDataResponse($dataCity);
            } else {
                $externalDataCity = $this->weatherService->getExternalWeather($query);
                if (isset($externalDataCity->error)) {
                    return missingQueryResponse();
                } else {
                    $newDataCity = $this->weatherService->insertWeatherData($externalDataCity);
                    $response = formatDataResponse($newDataCity);
                }
            }
        }

        return response()->json($response);
    }
}
