<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherCity extends Model
{
    use HasFactory;

    protected $fillable = ['type','language','unit','name','country_id','region_id','lat','lon','timezone_id','localtime','localtime_epoch','utc_offset','observation_time','temperature',
                            'weather_code','weather_icons','weather_descriptions','wind_speed','wind_degree','wind_dir','pressure','precip','humidity','cloudcover','feelslike','uv_index',
                            'visibility','is_day'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
