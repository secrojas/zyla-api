<?php

use App\Models\WeatherCity;
use Carbon\Carbon;
use App\Models\Country;
use App\Models\Region;

function missingQueryResponse()
{
    $response = [
        'success'=> false,
        'error' => [
            'code' => 601,
            'type' => 'missing_query',
            'info' =>'Please specify a valid location identifier using the query parameter.'
        ]

    ];

    return response()->json($response);
}

function formatDataResponse(WeatherCity $city)
{
    $data = [
        'request' => [
            'type' => $city->type,
            'query' =>  $city->region->name .', '.$city->country->name,
            'language' =>  $city->language,
            'unit' =>  $city->unit
        ],
        'location' => [
            'name' => $city->name,
            'country' => $city->country->name,
            'region' => $city->region->name,
            'lat' => $city->lat,
            'lon' => $city->lon,
            'timezone_id' => $city->timezone_id,
            'localtime' => $city->localtime,
            'localtime_epoch' => $city->localtime_epoch,
            'utc_offset' => $city->utc_offset,
        ],
        'current' => [
            'observation_time' => $city->observation_time,
            'temperature' =>$city->temperature,
            'weather_code' => $city->weather_code,
            'weather_icons' => [
                $city->weather_icons
            ],
            'weather_descriptions' => [
                $city->weather_descriptions
            ],
            'wind_speed' => $city->wind_speed,
            'wind_degree' => $city->wind_degree,
            'wind_dir' => $city->wind_dir,
            'pressure' => $city->pressure,
            'precip' => $city->precip,
            'humidity' => $city->humidity,
            'cloudcover' => $city->cloudcover,
            'feelslike' => $city->feelslike,
            'uv_index' => $city->uv_index,
            'visibility' => $city->visibility,
            'is_day' => $city->is_day
        ]
    ];

    return $data;
}

function validateHours(WeatherCity $city)
{
    $now = Carbon::now();

    $dayDataCity = $city->updated_at->toDateTimeString();

    if ($now->diffInMinutes($dayDataCity) > 60) {
        return false;
    } else {
        return true;
    }
}

function validateCountry(string $name): Country
{
    $country = Country::where('name', $name)->first();

    if (!$country) {
        $countryData = Country::Create([
            'name' => $name
        ]);
    } else {
        $countryData =  $country;
    }

    return $countryData;
}

function validateRegion(string $name): Region
{
    $region = Region::where('name', $name)->first();

    if (!$region) {
        $regionData = Region::Create([
            'name' => $name
        ]);
    } else {
        $regionData =  $region;
    }

    return $regionData;
}
