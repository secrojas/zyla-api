<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_cities', function (Blueprint $table) {
            $table->id();
            $table->string('type', 15);
            $table->string('language', 15);
            $table->string('unit', 15);
            $table->string('name', 150);
            $table->bigInteger('country_id')->unsigned();
            $table->bigInteger('region_id')->unsigned();
            $table->string('lat', 15);
            $table->string('lon', 15);
            $table->string('timezone_id', 150);
            $table->dateTime('localtime');
            $table->string('localtime_epoch', 150);
            $table->string('utc_offset', 15);
            $table->string('observation_time', 15);
            $table->bigInteger('temperature');
            $table->bigInteger('weather_code');
            $table->string('weather_icons', 250);
            $table->string('weather_descriptions', 150);
            $table->bigInteger('wind_speed');
            $table->bigInteger('wind_degree');
            $table->string('wind_dir', 15);
            $table->bigInteger('pressure');
            $table->bigInteger('precip');
            $table->bigInteger('humidity');
            $table->bigInteger('cloudcover');
            $table->bigInteger('feelslike');
            $table->bigInteger('uv_index');
            $table->bigInteger('visibility');
            $table->string('is_day', 15);
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('region_id')->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_cities');
    }
};
